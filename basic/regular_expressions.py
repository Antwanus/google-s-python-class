#!/usr/bin/python2.7 -tt
import re # re = regular expressions
# .  => any char (space, colon, comma,...)
# \w => word char (detects if it's a string, username_123 = word)
# \d => digit (detects numbers)
# \s => whitespace
# \S => non-whitespace (capital 'S')
# +  => 1 or more
# *  => 0 or more

def main():
  Find('ig', 'called piig')
  Find('...g', 'called piiig')
  Find('..g', 'called piiig xig')
  Find(r'c\.l', 'c.lled piiig') # r'' => raw'string'  ==> do NOT do any special processing with backslashes, just send it through raw
  Find(r':\w\w\w', 'called :pig')
  Find(r'\d\d\d', 'called user123')
  Find(r'ed\spig', 'called pig')
  Find(r'\d\s+\d\s+\d', '1       2              3')
  Find(r':\w+', 'blah blah blah :kitten blah blah')                             # FOUND:  :kitten
  Find(r':\S+', 'blah blah blah :kitten&123@#{[]} blah blah')                   # FOUND:  :kitten&123@#{[]}
  # the '.' in [ ] does not require the escape symbol \
  Find(r'[\w.]+@[\w.]+', 'contact @ me at meee.meee@meeeeee.me take care bye')  # FOUND:  meee.meee@meeeeee.me

  Find(r'[\w.]+@[\w.]+', 'contact @ me at meee.meee@meeeeee.me take care bye')  # FOUND:  meee.meee@meeeeee.me
  Find(r'\w[\w.]*@[\w.]', 'contact @ .me.me@me.me' )


# def Find(pattern, text):
#   matchObject = re.search(pattern, text)
#   if matchObject: print('FOUND: ', matchObject.group())
#   else: print ('NOT FOUND :(')

def Find(pattern, text):
  matchObject = re.search(pattern, text)
  if matchObject: return matchObject.group()


if __name__ == '__main__':
  main()