#!/usr/bin/python2.7
import os
import sys
import shutil

def List(dir):
    filenamesList = os.listdir(dir)
    print filenamesList
    for filename in filenamesList:
        path = os.path.join(dir, filename)
        print path
        print os.path.abspath(path)
    print '/tmp/foo exists? => ', str(os.path.exists('/tmp/foo'))

    # shutil.copy(source, dest)

def main():
    # print dir(os)
    # print help(os.listdir)
    List(sys.argv[1])

if __name__ == '__main__':
  main()
